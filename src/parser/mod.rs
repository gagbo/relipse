use crate::RelError;
use crate::{RelResult, RelRet, RelVal};
use pest::{iterators::Pair, Parser};

#[cfg(debug_assertions)]
const _GRAMMAR: &str = include_str!("elisp.pest");

#[derive(Parser)]
#[grammar = "parser/elisp.pest"]
pub struct RelParser;

pub fn parse(content: &str) -> RelRet {
    let parsed = RelParser::parse(Rule::elisp, content)
        .map_err(|err| RelError::ParseError(Box::new(err)))?
        .next()
        .unwrap();
    lex(parsed)
}

fn lex(parsed: Pair<Rule>) -> RelRet {
    match parsed.as_rule() {
        Rule::elisp => {
            let mut ret = RelVal::vector();
            lex_to_containing_val(&mut ret, parsed)?;
            Ok(ret)
        }
        Rule::t => Ok(RelVal::T),
        Rule::nil => Ok(RelVal::Nil),
        Rule::expr => lex(parsed.into_inner().next().unwrap()),
        Rule::sexpr => {
            let mut ret = RelVal::list();
            lex_to_containing_val(&mut ret, parsed)?;
            Ok(ret)
        }
        Rule::num => Ok(RelVal::num(
            parsed
                .as_str()
                .parse::<i64>()
                .map_err(|err| RelError::ParseError(Box::new(err)))?,
        )),
        Rule::symbol => Ok(RelVal::symbol(parsed.as_str())),
        _ => unreachable!(), // COMMENT/WHITESPACE etc
    }
}

fn lex_to_containing_val(v: &mut RelVal, parsed: Pair<Rule>) -> RelResult<()> {
    for child in parsed.into_inner() {
        if is_bracket_or_eoi(&child) {
            continue;
        }
        v.add(&lex(child)?)?;
    }
    Ok(())
}

fn is_bracket_or_eoi(parsed: &Pair<Rule>) -> bool {
    if parsed.as_rule() == Rule::EOI {
        return true;
    }
    let brackets = vec!["(", ")", "{", "}"];
    let c = parsed.as_str();
    brackets.contains(&c)
}

#[cfg(test)]
mod tests {
    use super::*;
    use crate::vector;

    #[test]
    fn parse_bool() {
        let parsed = RelParser::parse(Rule::elisp, "t").unwrap().next().unwrap();
        let lexed = lex(parsed).unwrap();
        assert_eq!(vector!(RelVal::symbol("t".into())), lexed);
    }
}
