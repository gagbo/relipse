use crate::{RelEnv, RelError, RelRet, RelVal, RelVector};

// TODO: Finish a real evaluator
pub fn eval(val: RelVal, env: &RelEnv) -> RelRet {
    match val {
        RelVal::Nil | RelVal::T | RelVal::Fixnum(_) => Ok(val),
        RelVal::List(ref args) => match args.car() {
            RelVal::Nil => {
                return Ok(RelVal::Nil);
            }
            ref fun => {
                let function_name = if let RelVal::Symbol(name) = fun {
                    name.to_string()
                } else {
                    match eval(fun.clone(), env)? {
                        RelVal::Symbol(name) => name,
                        _ => {
                            return Err(RelError::ErrString(format!(
                                "{} is not a symbol",
                                fun.to_string()
                            )))
                        }
                    }
                };
                let fun = env.function(&function_name)?;
                if let RelVal::Lambda(fun) = fun {
                    fun.eval(args, env)
                } else {
                    Err(RelError::ErrString(
                        "Unreachable code in eval : env.function returned an Ok(not Lambda)".into(),
                    ))
                }
            }
        },
        RelVal::Vector(ref elems) => Ok(RelVal::vector_from_elems(
            elems
                .iter()
                .map(|elem| eval(elem.clone(), env))
                .collect::<Result<RelVector, _>>()?,
        )),
        RelVal::Symbol(ref symbol) => env.variable(symbol),
        RelVal::Lambda(_) => Err(RelError::ErrString(
            "cannot evaluate a lambda outside a list context".into(),
        )),
    }
}
