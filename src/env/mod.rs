use std::collections::HashMap;
use std::rc::Rc;

use crate::builtins;
use crate::{RelError, RelLambda, RelRet, RelVal};

/// An environment of bindings between symbols (described as strings),
/// and variables or functions.
///
/// Elisp has exactly 2 namespaces: one for the variables,
/// and one for the functions.
#[derive(Debug)]
pub struct RelEnv<'a> {
    functions: HashMap<&'a str, RelVal>,
    variables: HashMap<&'a str, RelVal>,
    parent: Option<Rc<RelEnv<'a>>>,
}

impl<'a> Default for RelEnv<'a> {
    fn default() -> Self {
        let mut functions = HashMap::new();
        functions.insert(
            "id",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::Builtin(builtins::identity),
                docstring: "return the arguments unchanged".into(),
                binds: Vec::new(),
            }),
        );
        functions.insert(
            "numberp",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::Builtin(builtins::numberp),
                docstring: "return t if the OBJECT is a number (floating point or integer)".into(),
                binds: Vec::new(),
            }),
        );
        functions.insert(
            "car",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::Builtin(builtins::car),
                docstring: "return the car of OBJECT".into(),
                binds: Vec::new(),
            }),
        );
        functions.insert(
            "cdr",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::Builtin(builtins::cdr),
                docstring: "return the cdr of OBJECT".into(),
                binds: Vec::new(),
            }),
        );
        functions.insert(
            "cadr",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::Builtin(builtins::cadr),
                docstring: "return the cadr of OBJECT".into(),
                binds: Vec::new(),
            }),
        );
        functions.insert(
            "cddr",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::Builtin(builtins::cddr),
                docstring: "return the cddr of OBJECT".into(),
                binds: Vec::new(),
            }),
        );
        functions.insert(
            "lambda",
            RelVal::Lambda(RelLambda {
                func: crate::InnerFunc::SpecialBuiltin(builtins::lambda),
                docstring: "return an anonymous procedure".into(),
                binds: Vec::new(),
            }),
        );
        let mut variables = HashMap::new();
        variables.insert("t", RelVal::t());
        variables.insert("nil", RelVal::nil());
        Self {
            functions,
            variables,
            parent: None,
        }
    }
}

impl<'a> RelEnv<'a> {
    /// Return the variable in the environment
    pub fn variable(&self, variable_name: &str) -> RelRet {
        self.variables
            .get::<str>(variable_name)
            .ok_or_else(|| RelError::UnknownVariable(variable_name.to_string()))
            .map(Clone::clone)
    }

    /// Return the function in the environment
    pub fn function(&self, function_name: &str) -> RelRet {
        self.functions
            .get::<str>(function_name)
            .ok_or_else(|| RelError::UnknownFunction(function_name.to_string()))
            .map(Clone::clone)
    }
}
