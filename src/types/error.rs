use super::RelVal;

#[derive(Debug)]
pub struct WrongNumberArgs {
    pub min: u8,
    pub max: u8,
    pub actual: u8,
}

#[derive(Debug)]
pub enum RelError {
    ErrString(String),
    ParseError(Box<dyn std::error::Error>),
    NoChildren(RelVal),
    UnknownVariable(String),
    UnknownFunction(String),
    WrongNumberArgs(WrongNumberArgs),
}

impl std::fmt::Display for RelError {
    fn fmt(&self, f: &mut std::fmt::Formatter) -> std::fmt::Result {
        match self {
            Self::ErrString(err) => write!(f, "generic error: {}", err),
            Self::NoChildren(val) => write!(f, "this value has no children: {}", val.to_string()),
            Self::ParseError(err) => write!(f, "parsing error: {}", err),
            Self::UnknownVariable(symbol) => write!(f, "unknown variable: {}", symbol),
            Self::UnknownFunction(symbol) => write!(f, "unknown function: {}", symbol),
            Self::WrongNumberArgs(inner) => write!(
                f,
                "wrong number of arguments: ({} . {}) {}",
                inner.min, inner.max, inner.actual
            ),
        }
    }
}

impl std::error::Error for RelError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::ErrString(_)
            | Self::NoChildren(_)
            | Self::UnknownVariable(_)
            | Self::UnknownFunction(_)
            | Self::WrongNumberArgs(_) => None,
            Self::ParseError(err) => Some(&**err),
        }
    }
}

impl RelError {
    pub fn wrong_number_args(min: u8, max: u8, actual: u8) -> Self {
        Self::WrongNumberArgs(WrongNumberArgs { min, max, actual })
    }
}
