use crate::RelError;

use super::RelVal;
use std::convert::{TryFrom, TryInto};
use std::iter::FromIterator;
use std::rc::Rc;

#[derive(Debug, Clone)]
enum InnerRelList {
    Cons(Rc<RelVal>, Rc<InnerRelList>),
    Nil,
}

impl InnerRelList {
    /// Helper recursive function that modifies `self` in place to
    /// append a value to the end of the list
    pub fn append_mut(&mut self, val: RelVal) {
        match self {
            InnerRelList::Cons(_, cdr) => (*Rc::make_mut(cdr)).append_mut(val),
            InnerRelList::Nil => *self = Self::Cons(Rc::new(val), Rc::new(InnerRelList::Nil)),
        }
    }
}

impl From<Rc<InnerRelList>> for RelList {
    fn from(inner: Rc<InnerRelList>) -> Self {
        Self { inner }
    }
}

/// Inner structure holding an immutable list value.
///
/// A list is basically a simply linked list of cons cells.
///
/// Since lists are immutable, std::rc::Rc is used to share memory allocations
///
/// Any "mutating" operation is handled with Rc::make_mut which implements
/// a "clone on write" strategy
#[derive(Default, Debug, Clone)]
pub struct RelList {
    inner: Rc<InnerRelList>,
}

impl RelList {
    /// Return the car of the list
    pub fn car(&self) -> RelVal {
        match &*self.inner {
            InnerRelList::Cons(car, _) => (**car).clone(),
            InnerRelList::Nil => RelVal::Nil,
        }
    }

    /// Return the cdr of the list.
    /// Guaranteed to be RelVal::Nil or a RelVal::List
    pub fn cdr(&self) -> RelVal {
        match &*self.inner {
            InnerRelList::Cons(_, cdr) => RelVal::List(cdr.clone().into()),
            InnerRelList::Nil => RelVal::Nil,
        }
    }

    /// Return a list with val pushed in front
    pub fn push(&self, val: RelVal) -> Self {
        Self::cons(val, self.clone())
    }

    // FIXME: this mutates the argument list and this is bad.
    // needs a test
    /// Return a list with val added to the end of it.
    pub fn append(&self, val: RelVal) -> Self {
        let mut retval = self.clone();
        Rc::make_mut(&mut retval.inner).append_mut(val);
        retval
    }

    /// Construct a new list from a value and a tail for the list
    pub fn cons(car: RelVal, cdr: Self) -> Self {
        Self {
            inner: Rc::new(InnerRelList::Cons(Rc::new(car), cdr.inner.clone())),
        }
    }

    /// Return a borrowed iterator over list items
    pub fn iter(&self) -> <Self as IntoIterator>::IntoIter {
        ListIterator(self.clone())
    }

    /// Return the length of the list
    pub fn len(&self) -> usize {
        self.iter().count()
    }

    /// Return a fresh copy of the list
    pub fn deep_clone(&self) -> Self {
        self.clone()
    }
}

impl From<Rc<RelList>> for RelList {
    fn from(list_pointer: Rc<RelList>) -> Self {
        Self {
            inner: list_pointer.inner.clone(),
        }
    }
}

impl Default for InnerRelList {
    fn default() -> Self {
        Self::Nil
    }
}

impl TryFrom<RelVal> for RelList {
    type Error = RelError;

    fn try_from(value: RelVal) -> Result<Self, Self::Error> {
        match value {
            RelVal::List(inner) => Ok(inner),
            RelVal::Nil
            | RelVal::T
            | RelVal::Fixnum(_)
            | RelVal::Symbol(_)
            | RelVal::Lambda(_)
            | RelVal::Vector(_) => Err(RelError::ErrString(format!(
                "not a list : {}",
                value.to_string()
            ))),
        }
    }
}

impl From<RelList> for RelVal {
    fn from(inner: RelList) -> Self {
        Self::List(inner)
    }
}

/// Iterator over a List structure
/// as list are immutable, the type is a simple wrapper that advances the car of the list
pub struct ListIterator(RelList);

impl Iterator for ListIterator {
    type Item = RelVal;
    fn next(&mut self) -> Option<Self::Item> {
        if let InnerRelList::Nil = *self.0.inner {
            return None;
        }
        let val = self.0.car();
        *self = Self(self.0.cdr().try_into().unwrap());
        Some(val)
    }
}

impl IntoIterator for RelList {
    type Item = RelVal;

    type IntoIter = ListIterator;

    fn into_iter(self) -> Self::IntoIter {
        ListIterator(self)
    }
}

impl FromIterator<RelVal> for RelList {
    fn from_iter<T: IntoIterator<Item = RelVal>>(iter: T) -> Self {
        let mut retval = RelList::default();
        for item in iter {
            retval = retval.append(item)
        }
        retval
    }
}

#[cfg(test)]
mod tests {
    use crate::list;

    use super::*;

    #[test]
    fn append_does_not_mutate() {
        let test_list = RelList::try_from(list!(RelVal::T, RelVal::T,)).unwrap();
        let old_len = test_list.len();
        let appended_list = test_list.append(RelVal::Fixnum(3));
        let new_len = appended_list.len();
        assert_eq!(test_list.len(), old_len);
        assert_eq!(appended_list.len(), new_len);
        assert_eq!(new_len, old_len + 1);
    }
}
