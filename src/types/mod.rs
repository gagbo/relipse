use itertools::Itertools;

mod error;
pub use error::RelError;

mod functions;
pub use functions::{InnerFunc, RelLambda};

mod list;
pub use list::RelList;

// TODO: move List and Vector out of RelType
// rebuild types to limit indirections
// Have only primitive types in RelType ?
// RelVal could hold all other including List Vector Lambda Buffer…
#[derive(Debug, Clone)]
pub enum RelVal {
    Nil,
    T,
    Fixnum(i64),
    Symbol(String),
    Lambda(RelLambda),
    List(RelList),
    Vector(RelVector),
}

pub type RelVector = Vec<RelVal>;
pub type RelResult<T> = Result<T, RelError>;
pub type RelRet = RelResult<RelVal>;

// type utility macros
#[allow(unused_macros)]
#[macro_export]
macro_rules! list {
    () => {RelList::default()};
  [$car:expr, $($cdr:tt)*] => {{
      use std::convert::TryInto;
      let v: crate::RelList = RelList::cons(
          $car,
          list!($($cdr)*).try_into().unwrap()
      );
    crate::RelVal::List(v)
  }}
}

#[allow(unused_macros)]
#[macro_export]
macro_rules! vector {
  [$arg:expr] => {{
    let v: crate::RelVector = vec![$arg];
    crate::RelVal::Vector(v)
  }};
  [$($args:expr),*] => {{
    let v: crate::RelVector = vec![$($args.clone(),) *];
    crate::RelVal::Vector(v)
  }}
}

// type utility functions

pub fn error(s: &str) -> RelError {
    RelError::ErrString(s.to_string())
}

impl PartialEq for RelVal {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Self::Nil, Self::Nil) => true,
            (Self::T, Self::T) => true,
            (Self::Vector(ref inner_a), Self::Vector(ref inner_b)) => {
                inner_a.len() == inner_b.len()
                    && inner_a.iter().zip(inner_b.iter()).all(|(a, b)| a == b)
            }
            (Self::List(ref inner_a), Self::List(ref inner_b)) => {
                inner_a.len() == inner_b.len()
                    && inner_a.iter().zip(inner_b.iter()).all(|(a, b)| a == b)
            }
            (Self::Symbol(ref name_1), Self::Symbol(ref name_2)) => name_1 == name_2,
            _ => false,
        }
    }
}

// Representation

impl ToString for RelVal {
    fn to_string(&self) -> String {
        match self {
            Self::Nil => "nil".into(),
            Self::T => "t".into(),
            Self::List(ref inner) => {
                format!("({})", inner.iter().map(|val| val.to_string()).join(" "),)
            }
            Self::Vector(ref inner) => {
                format!("[{}]", inner.iter().map(|val| val.to_string()).join(" "),)
            }
            Self::Fixnum(ref val) => format!("{}", val),
            Self::Symbol(ref name) => format!("{}", name),
            RelVal::Lambda(ref proc) => match proc.func {
                InnerFunc::Lambda(_) => "#<function>".into(),
                InnerFunc::Builtin(_) => "#<subr>".into(),
                InnerFunc::SpecialBuiltin(_) => "#<subr>".into(),
                InnerFunc::SpecialLambda(_) => "#<function>".into(),
            },
        }
    }
}

impl RelVal {
    pub fn add(&mut self, x: &RelVal) -> RelResult<()> {
        match self {
            Self::Vector(ref mut inner) => {
                inner.push(x.clone());
            }
            Self::List(ref mut inner) => {
                *inner = inner.append(x.clone());
            }
            _ => return Err(RelError::NoChildren(self.clone())),
        }
        Ok(())
    }

    // Extract single element of sexpr at index i
    pub fn pop(&mut self, _i: usize) -> RelRet {
        match self {
            _ => Err(RelError::NoChildren(self.clone())),
        }
    }

    pub fn list() -> Self {
        Self::List(RelList::default())
    }

    pub fn list_from_elems(elems: RelVector) -> Self {
        let mut ret = RelList::default();
        for val in elems.into_iter().rev() {
            ret = ret.push(val);
        }
        Self::List(ret)
    }

    pub fn vector() -> Self {
        Self::Vector(Vec::new())
    }

    pub fn vector_from_elems(elems: RelVector) -> Self {
        Self::Vector(elems)
    }

    pub fn num(val: i64) -> Self {
        Self::Fixnum(val).into()
    }

    pub fn symbol(val: &str) -> Self {
        Self::Symbol(val.to_string()).into()
    }

    pub fn t() -> Self {
        Self::T
    }

    pub fn nil() -> Self {
        Self::Nil
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn equality() {
        assert_eq!(RelVal::symbol("t".into()), RelVal::symbol("t".into()));
        assert_ne!(RelVal::symbol("n".into()), RelVal::symbol("t".into()));
        assert_eq!(
            list!(RelVal::t(), RelVal::t(),),
            list!(RelVal::t(), RelVal::t(),)
        );
    }

    #[test]
    fn print() {
        let rel_true = RelVal::t();
        let rel_false = RelVal::nil();
        assert_eq!(rel_true.to_string(), "t".to_string());
        assert_eq!(rel_false.to_string(), "nil".to_string());
        assert_eq!(RelVal::nil().to_string(), "nil".to_string());

        assert_eq!(
            list!(RelVal::t(), RelVal::t(),).to_string(),
            "(t t)".to_string()
        );
        assert_eq!(
            vector!(
                RelVal::t(),
                RelVal::t(),
                list!(RelVal::t(), RelVal::t(), RelVal::t(),)
            )
            .to_string(),
            "[t t (t t t)]".to_string()
        );
        assert_eq!(
            vector![RelVal::List(crate::RelList::default())].to_string(),
            "[()]".to_string()
        );
    }
}
