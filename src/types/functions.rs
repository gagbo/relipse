use crate::{eval, RelEnv, RelError, RelList, RelRet, RelVector};

use std::convert::TryFrom;
use std::rc::Rc;

#[derive(Clone)]
pub enum InnerFunc {
    Lambda(Rc<dyn Fn(RelVector) -> RelRet>),
    Builtin(fn(RelVector) -> RelRet),
    SpecialBuiltin(fn(RelList) -> RelRet),
    SpecialLambda(Rc<dyn Fn(RelList) -> RelRet>),
}

#[derive(Clone)]
pub struct RelLambda {
    /// the inner procedure to call
    pub func: InnerFunc,
    /// Docstring for the closure
    pub docstring: String,
    /// Names for the variables (&rest is assumed to be the last name)
    pub binds: Vec<String>,
}

impl RelLambda {
    /// Evaluate a function/macro call in the given environment.
    /// args should be the initially given list
    /// (i.e. bindings must be done on `(cdr args)`)
    pub fn eval(&self, args: &RelList, env: &RelEnv) -> RelRet {
        // TODO check if fun is a macro (so dummy false for now)
        // TODO evaluate all the remaining args recursively
        // TODO build the evaluated arg list
        match &self.func {
            InnerFunc::Builtin(raw_fn) => {
                let evaled_args: crate::RelVector = crate::RelList::try_from(args.cdr())
                    .unwrap()
                    .iter()
                    .map(|val| eval::eval(val, env))
                    .collect::<Result<crate::RelVector, RelError>>()?;
                raw_fn(evaled_args)
            }
            InnerFunc::Lambda(_boxed_fn) => {
                // TODO : handle boxed functions
                Err(RelError::ErrString(
                    "non builtin functions are unimplemented".into(),
                ))
            }
            InnerFunc::SpecialBuiltin(raw_fn) => {
                // Evaluate the inner function without touching the arguments
                // The environment should be passed as well for things like setq/setf
                raw_fn(args.clone())
            }
            InnerFunc::SpecialLambda(_) => {
                // TODO : handle boxed functions
                Err(RelError::ErrString(
                    "non builtin functions are unimplemented".into(),
                ))
            }
        }
    }
}

impl std::fmt::Debug for RelLambda {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "lambda : Docstring = {:#?}", self.docstring)
    }
}
