use relipse::{eval::eval, parser::parse, RelEnv};

use std::{
    env,
    fs::File,
    io::{BufReader, Read},
};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let args: Vec<String> = env::args().collect();

    let file = File::open(args.get(1).unwrap())?;
    let mut buf_reader = BufReader::new(file);
    let mut contents = String::new();
    buf_reader.read_to_string(&mut contents)?;

    let env = RelEnv::default();

    let res = parse(&contents).and_then(|elisp_val| eval(elisp_val, &env));
    match res {
        Ok(val) => {
            println!("{}", val.to_string());
            Ok(())
        }
        Err(err) => {
            println!("error: {}", err);
            Err(Box::new(err))
        }
    }
}
