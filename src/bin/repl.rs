use relipse::{eval::eval, parser::parse, RelEnv};
use rustyline::{error::ReadlineError, Editor};

fn main() -> Result<(), Box<dyn std::error::Error>> {
    // `()` can be used when no completer is required
    let mut rl = Editor::<()>::new();
    if rl.load_history(".relipse-history").is_err() {
        eprintln!("No previous history.");
    }

    let env = RelEnv::default();
    eprintln!("Starting with environment : {:#?}", env);

    loop {
        let readline = rl.readline("user> ");
        match readline {
            Ok(line) => {
                rl.add_history_entry(&line);
                rl.save_history(".relipse-history").unwrap();
                let res = parse(&line).and_then(|elisp_val| eval(elisp_val, &env));
                match res {
                    Ok(val) => println!("{}", val.to_string()),
                    Err(err) => println!("error: {}", err),
                }
            }
            Err(ReadlineError::Interrupted) => continue,
            Err(ReadlineError::Eof) => break Ok(()),
            Err(err) => {
                println!("Error: {:?}", err);
                break Err(Box::new(err));
            }
        }
    }
}
