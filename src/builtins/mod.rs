use std::{convert::TryFrom, rc::Rc};

use crate::{InnerFunc, RelError, RelLambda, RelList, RelRet, RelVal, RelVector};

pub fn identity(args: RelVector) -> RelRet {
    Ok(RelVal::list_from_elems(args))
}

pub fn car(args: RelVector) -> RelRet {
    if args.len() < 1 || args.len() > 1 {
        return Err(RelError::wrong_number_args(1, 1, args.len() as u8));
    }
    match args[0] {
        RelVal::List(ref inner) => Ok(inner.car()),
        _ => Err(RelError::ErrString(format!(
            "Wrong type: listp {}",
            args[0].to_string()
        ))),
    }
}

pub fn cdr(args: RelVector) -> RelRet {
    if args.len() < 1 || args.len() > 1 {
        return Err(RelError::wrong_number_args(1, 1, args.len() as u8));
    }
    match args[0] {
        RelVal::List(ref inner) => Ok(inner.cdr()),
        _ => Err(RelError::ErrString(format!(
            "Wrong type: listp {}",
            args[0].to_string()
        ))),
    }
}

pub fn cadr(args: RelVector) -> RelRet {
    if args.len() < 1 || args.len() > 1 {
        return Err(RelError::wrong_number_args(1, 1, args.len() as u8));
    }
    match args[0] {
        RelVal::List(ref inner) => Ok(crate::RelList::try_from(inner.cdr()).unwrap().car()),
        _ => Err(RelError::ErrString(format!(
            "Wrong type: listp {}",
            args[0].to_string()
        ))),
    }
}

pub fn cddr(args: RelVector) -> RelRet {
    if args.len() < 1 || args.len() > 1 {
        return Err(RelError::wrong_number_args(1, 1, args.len() as u8));
    }
    match args[0] {
        RelVal::List(ref inner) => Ok(crate::RelList::try_from(inner.cdr()).unwrap().cdr()),
        _ => Err(RelError::ErrString(format!(
            "Wrong type: listp {}",
            args[0].to_string()
        ))),
    }
}

pub fn numberp(args: RelVector) -> RelRet {
    if args.len() < 1 || args.len() > 1 {
        return Err(RelError::wrong_number_args(1, 1, args.len() as u8));
    }
    match args[0] {
        RelVal::Fixnum(_) => Ok(RelVal::T),
        _ => Ok(RelVal::Nil),
    }
}

/// Build a lambda from a lambda expression
///
/// [Source](https://www.gnu.org/software/emacs/manual/html_node/elisp/Lambda-Components.html#Lambda-Components)
pub fn lambda(lambda_form: RelList) -> RelRet {
    //   (lambda (arg-variables…)
    // [documentation-string]
    // [interactive-declaration]
    // body-forms…)

    let binds: Vec<String> =
        crate::RelList::try_from(cadr(vec![RelVal::List(lambda_form.clone())])?)
            .unwrap()
            .iter()
            .map(|val| val.to_string())
            .collect();
    // TODO: add trace for the binds

    let bodies: Vec<RelVal> = crate::RelList::try_from(cddr(vec![RelVal::List(lambda_form)])?)
        .unwrap()
        .iter()
        .map(|body_form| body_form)
        .collect();
    // TODO: add trace for the bodies

    // Ignore Interactive and Documentation strings for now

    // FIXME dummy retval
    let retval = RelLambda {
        func: InnerFunc::Lambda(Rc::new(|_| Ok(RelVal::t()))),
        docstring: "".into(),
        binds: Vec::new(),
    };
    Ok(RelVal::Lambda(retval))
}
