#[macro_use]
extern crate pest_derive;

pub mod builtins;
pub mod env;
pub mod eval;
pub mod parser;
#[macro_use]
pub mod types;

pub use env::*;
pub use eval::*;
pub use parser::*;
pub use types::*;

// TODO: REPL https://www.gnu.org/software/emacs/manual/html_node/elisp/Printed-Representation.html#Printed-Representation
// Then numbers
// Then characters and strings
