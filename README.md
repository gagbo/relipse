# Relipse, a Rust implemented emacs-lisp

## Test
`cargo run -p repl` will start the REPL

## Goal
The goal of this project is to try and see how to implement a more complex lisp in Rust

## Roadmap
They mostly are a mix of "setting up a REPL env to test live" and "following the
[emacs-lisp reference manual](https://www.gnu.org/software/emacs/manual/html_node/elisp/index.html#SEC_Contents)

### Implement basic REPL

### Add basic types

### Add complex types
