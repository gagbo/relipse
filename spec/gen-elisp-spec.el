;;; gen-elisp-spec --- Generate a micro elisp spec from emacs -*- lexical-binding: t; -*-
;;
;;; Commentary:
;; All this does is check for symbol existence.
;;
;; Generate:
;; emacs -Q --batch --script gen-elisp-spec.el > elisp-spec.el
;; Check:
;; time emacs -Q --batch --script elisp-spec.el
;; time cargo run --bin relipse -- elisp-spec.el
;;
;; Thanks to wasamasa for the snippet.
;; Relevant blogpost : https://emacsninja.com/posts/state-of-emacs-lisp-on-guile.html

;;; Code:
(defun printf (fmt &rest args)
  "Printf implementation.
Format FMT using ARGS to display a formatted string."
  (princ (apply 'format fmt args)))

(printf ";; elisp spec adherence test
(defvar passed 0)
(defvar failed 0)
(defun test-sym (pred sym)
  (if (funcall pred sym)
      (setq passed (1+ passed))
    (setq failed (1+ failed))))
(defun test-fun (sym) (test-sym 'fboundp sym))
(defun test-var (sym) (test-sym 'boundp sym))\n\n")

(mapatoms
 (lambda (atom)
   (when (fboundp atom)
     (printf "(test-fun '%S)\n" atom))
   (when (and (not (keywordp atom)) (boundp atom))
     (printf "(test-var '%S)\n" atom))))

(printf "\n")
(printf "(princ \"Passed: \")\n")
(printf "(princ passed)\n")
(printf "(terpri)\n")
(printf "\n")
(printf "(princ \"Failed: \")\n")
(printf "(princ failed)\n")
(printf "(terpri)\n")

(provide 'gen-elisp-spec)
;;; gen-elisp-spec.el ends here
